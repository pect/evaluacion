package mx.com.praxis.controllers.utils;

public class Constant {
    
    private Constant() {}

    public static final String ACTION = "action";
    
    public static final String ID = "id";
    public static final String ID_LIST = "id_list";
    public static final String ID_TASK = "id_task";

    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String IS_COMPLETED = "isCompleted";

    public static final String PAGE = "page";
    public static final String PAGE_DEFAULT = "1";
    public static final String SIZE = "size";
    public static final String SIZE_DEFAULT = "5";
    

    public static final String TASK = "task";
    public static final String NOTE = "note";
    public static final String TASK_LIST = "list";
    public static final String TASK_LISTS = "lists";

}
