package mx.com.praxis.controllers;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static mx.com.praxis.controllers.utils.Constant.*;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.dtos.TaskListDto;
import mx.com.praxis.services.TaskService;

@WebServlet("/tasks")
public class TaskController extends HttpServlet {

    private static final long serialVersionUID = 5906741178868445754L;
    
    @Inject
    TaskService taskService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameterMap().containsKey(ID)) {
            long id = Long.parseLong(req.getParameter(ID));
            
            TaskDto task = taskService.findById(id);
            req.setAttribute(TASK, task);
            req.getRequestDispatcher("/tasks/detail.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        Action action = Action.valueOf(req.getParameter("action"));
        TaskDto taskDto = null;
        
        switch (action) {
        case CREATE:
            taskDto = TaskDto
            .builder()
                .title(req.getParameter(TITLE))
                .taskList(TaskListDto
                        .builder()
                            .id(Long.parseLong(req.getParameter(ID_LIST)))
                        .build())
                .isCompleted(Boolean.parseBoolean(req.getParameter(IS_COMPLETED)))
            .build();
            
            Long id = taskService.insert(taskDto);

            resp.sendRedirect(req.getContextPath() + "/tasks?id=" + id);
            break;
            
        case REMOVE:
            taskService.remove(Long.parseLong(req.getParameter(ID)));

            resp.sendRedirect(req.getContextPath() + "/lists?id=" + req.getParameter(ID_LIST));
            break;
            
        case UPDATE:
            taskDto = TaskDto
            .builder()
                .id(Long.parseLong(req.getParameter(ID)))
                .title(req.getParameter(TITLE))
                .taskList(TaskListDto
                        .builder()
                        .id(Long.parseLong(req.getParameter(ID_LIST)))
                        .build())
                .isCompleted(Boolean.parseBoolean(req.getParameter(IS_COMPLETED)))
            .build();
            
            taskService.update(taskDto);
            
            resp.sendRedirect(req.getContextPath() + "/tasks?id=" + taskDto.getId());
            break;
        }
    }

}
