package mx.com.praxis.controllers;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static mx.com.praxis.controllers.utils.Constant.*;
import mx.com.praxis.dtos.NoteDto;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.services.NoteService;

@WebServlet("/notes")
public class NoteController extends HttpServlet {

    private static final long serialVersionUID = 7531800900458871471L;
    
    @Inject
    NoteService noteService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NoteDto note = noteService.findById(Long.parseLong(req.getParameter(ID)));
        req.setAttribute(NOTE, note);
        req.getRequestDispatcher("/notes/detail.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        Action action = Action.valueOf(req.getParameter(ACTION));
        NoteDto noteDto = null;
        
        switch (action) {
        case CREATE:
            noteDto = NoteDto
            .builder()
                .content(req.getParameter(CONTENT))
                .task(TaskDto
                        .builder()
                            .id(Long.parseLong(req.getParameter(ID_TASK)))
                        .build())
            .build();
            
            Long id = noteService.insert(noteDto);
            
            resp.sendRedirect(req.getContextPath() + "/notes?id=" + id);
            break;
            
        case REMOVE:
            noteService.remove(Long.parseLong(req.getParameter("id")));

            resp.sendRedirect(req.getContextPath() + "/tasks?id=" + req.getParameter("id_task"));
            break;
            
        case UPDATE:
            noteDto = NoteDto
            .builder()
                .id(Long.parseLong(req.getParameter(ID)))
                .content(req.getParameter(CONTENT))
                .task(TaskDto
                        .builder()
                            .id(Long.parseLong(req.getParameter(ID_TASK)))
                        .build())
            .build();
            
            noteService.update(noteDto);
            
            resp.sendRedirect(req.getContextPath() + "/notes?id=" + noteDto.getId());
            break;
        }
    }

}
