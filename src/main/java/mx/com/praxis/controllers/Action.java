package mx.com.praxis.controllers;

public enum Action {
    CREATE, UPDATE, REMOVE;
}
