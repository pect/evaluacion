package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static mx.com.praxis.controllers.utils.Constant.*;
import mx.com.praxis.dtos.TaskListDto;
import mx.com.praxis.services.TaskListService;

@WebServlet("/lists")
public class TaskListController extends HttpServlet{

    private static final long serialVersionUID = 5989704650354263132L;
    
    @Inject
    private TaskListService taskListService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameterMap().containsKey(ID)) {
            long id = Long.parseLong(req.getParameter(ID));
            
            TaskListDto list = taskListService.findById(id);
            req.setAttribute(TASK_LIST, list);
            req.getRequestDispatcher("/lists/detail.jsp").forward(req, resp);
        } else {
            Optional<String> optionalPage = Optional.ofNullable(req.getParameter(PAGE));
            Optional<String> optionalSize = Optional.ofNullable(req.getParameter(SIZE));
            
            int page = Integer.parseInt(optionalPage.orElse(PAGE_DEFAULT));
            int size = Integer.parseInt(optionalSize.orElse(SIZE_DEFAULT));
            
            List<TaskListDto> lists = taskListService.findAll(page, size);
            req.setAttribute(TASK_LISTS, lists);
            req.getRequestDispatcher("/lists/index.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        Action action = Action.valueOf(req.getParameter("action"));
        TaskListDto list = null;
        
        switch (action) {
        case CREATE:
            list = TaskListDto
            .builder()
                .title(req.getParameter(TITLE))
            .build();

            taskListService.insert(list);

            resp.sendRedirect(req.getContextPath() + "/lists");
            break;
        case REMOVE:
            taskListService.remove(Long.parseLong(req.getParameter(ID)));
            
            resp.sendRedirect(req.getContextPath() + "/lists");
            break;
        case UPDATE:
            list = TaskListDto
            .builder()
                .id(Long.parseLong(req.getParameter(ID)))
                .title(req.getParameter(TITLE))
            .build();
            
            taskListService.update(list);
            
//            req.setAttribute(TASK_LIST, list);
//            req.getRequestDispatcher("/lists/detail.jsp").forward(req, resp);
            resp.sendRedirect(req.getContextPath() + "/lists?id=" + list.getId());
            break;
        }
        
    }
    
    

}
