package mx.com.praxis.dtos;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDto {
    
    private long id;
    private String title;
    private boolean isCompleted;
    private TaskListDto taskList;
    @Builder.Default
    private List<NoteDto> notes = new ArrayList<>();

}
