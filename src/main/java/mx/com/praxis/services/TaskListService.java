package mx.com.praxis.services;

import mx.com.praxis.dtos.TaskListDto;

public interface TaskListService extends GenericService<TaskListDto, Long> {

}
