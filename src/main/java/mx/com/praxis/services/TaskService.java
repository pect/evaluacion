package mx.com.praxis.services;

import mx.com.praxis.dtos.TaskDto;

public interface TaskService extends GenericService<TaskDto, Long> {

}
