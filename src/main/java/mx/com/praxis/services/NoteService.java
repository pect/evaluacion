package mx.com.praxis.services;

import mx.com.praxis.dtos.NoteDto;

public interface NoteService extends GenericService<NoteDto, Long> {

}
