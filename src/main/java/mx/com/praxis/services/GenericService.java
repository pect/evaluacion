package mx.com.praxis.services;

import java.util.List;

public interface GenericService<E, K> {

    List<E> findAll(int page, int size);
    
    E findById(K key);
    
    void update(E entity);
    
    K insert(E entity);
    
    void remove(K key);
    
}
