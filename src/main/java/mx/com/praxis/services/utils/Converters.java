package mx.com.praxis.services.utils;

import java.util.stream.Collectors;

import mx.com.praxis.dtos.NoteDto;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.dtos.TaskListDto;
import mx.com.praxis.entities.Note;
import mx.com.praxis.entities.Task;
import mx.com.praxis.entities.TaskList;

public class Converters {
    
    private Converters() {}
    
    public static TaskListDto toDto(TaskList entity) {
        return TaskListDto.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .tasks(entity.getTasks()
                       .stream()
                       .map(task -> 
                           TaskDto.builder()
                               .id(task.getId())
                               .title(task.getTitle())
                               .isCompleted(task.isCompleted())
                           .build())
                       .collect(Collectors.toList()))
           .build();
    }
    
    public static TaskDto toDto(Task entity) {
        return TaskDto
               .builder()
                   .id(entity.getId())
                   .title(entity.getTitle())
                   .isCompleted(entity.isCompleted())
                   .taskList(TaskListDto
                           .builder()
                           .id(entity.getTaskList().getId())
                           .build())
                   .notes(entity.getNotes()
                       .stream()
                       .map(note -> NoteDto
                               .builder()
                                   .id(note.getId())
                                   .content(note.getContent())
                               .build())
                       .collect(Collectors.toList()))
               .build();
    }
    
    public static NoteDto toDto(Note entity) {
        return NoteDto.builder()
                .id(entity.getId())
                .content(entity.getContent())
                .task(TaskDto
                        .builder()
                            .id(entity.getTask().getId())
                            .taskList(TaskListDto
                                    .builder()
                                    .id(entity.getTask().getTaskList().getId())
                                    .build())
                        .build())
           .build();
    }
    
    public static TaskList toEntity(TaskListDto dto) {
        return TaskList.builder()
                .id(dto.getId())
                .title(dto.getTitle())
           .build();
    }

    public static Task toEntity(TaskDto dto) {
        TaskList taskList = TaskList
        .builder()
            .id(dto.getTaskList().getId())
        .build();
        
        return Task.builder()
                   .id(dto.getId())
                   .title(dto.getTitle())
                   .isCompleted(dto.isCompleted())
                   .taskList(taskList)
               .build();
    }
    
    public static Note toEntity(NoteDto dto) {
        Task task = Task
                .builder()
                    .id(dto.getTask().getId())
                .build();
                
                return Note.builder()
                            .id(dto.getId())
                            .content(dto.getContent())
                            .task(task)
                       .build();
    }
}
