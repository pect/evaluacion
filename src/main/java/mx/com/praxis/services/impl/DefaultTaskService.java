package mx.com.praxis.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.services.TaskService;
import static mx.com.praxis.services.utils.Converters.*;

@Stateless
public class DefaultTaskService implements TaskService {
    
    @Inject
    TaskDao taskDao;

    @Override
    public List<TaskDto> findAll(int page, int size) {
        return taskDao.findAll(page, size)
                .stream()
                .map(item -> toDto(item))
                .collect(Collectors.toList());
    }

    @Override
    public TaskDto findById(Long key) {
        return toDto(taskDao.findById(key));
    }

    @Override
    public void update(TaskDto entity) {
        taskDao.update(toEntity(entity));
    }

    @Override
    public Long insert(TaskDto entity) {
        return taskDao.insert(toEntity(entity));
    }

    @Override
    public void remove(Long key) {
        taskDao.remove(key);
    }

}
