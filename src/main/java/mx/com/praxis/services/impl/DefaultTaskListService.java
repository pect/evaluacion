package mx.com.praxis.services.impl;

import static mx.com.praxis.services.utils.Converters.toDto;
import static mx.com.praxis.services.utils.Converters.toEntity;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mx.com.praxis.daos.TaskListDao;
import mx.com.praxis.dtos.TaskListDto;
import mx.com.praxis.services.TaskListService;

@Stateless
public class DefaultTaskListService implements TaskListService {

    @Inject
    private TaskListDao taskListDao;
    
    @Override
    public List<TaskListDto> findAll(int page, int size) {
        return taskListDao
                .findAll(page, size)
                .stream()
                .map(item -> TaskListDto
                        .builder()
                        .id(item.getId())
                        .title(item.getTitle())
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public TaskListDto findById(Long key) {
        return toDto(taskListDao.findById(key));
    }

    @Override
    public void update(TaskListDto entity) {
        taskListDao.update(toEntity(entity));
    }

    @Override
    public Long insert(TaskListDto entity) {
        return taskListDao.insert(toEntity(entity));
    }

    @Override
    public void remove(Long key) {
        taskListDao.remove(key);
    }

}
