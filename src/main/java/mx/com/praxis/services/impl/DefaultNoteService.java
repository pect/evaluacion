package mx.com.praxis.services.impl;

import static mx.com.praxis.services.utils.Converters.toDto;
import static mx.com.praxis.services.utils.Converters.toEntity;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mx.com.praxis.daos.NoteDao;
import mx.com.praxis.dtos.NoteDto;
import mx.com.praxis.services.NoteService;

@Stateless
public class DefaultNoteService implements NoteService {

    @Inject
    NoteDao noteDao;

    @Override
    public List<NoteDto> findAll(int page, int size) {
        return noteDao.findAll(page, size)
                .stream()
                .map(item -> toDto(item))
                .collect(Collectors.toList());
    }

    @Override
    public NoteDto findById(Long key) {
        return toDto(noteDao.findById(key));
    }

    @Override
    public void update(NoteDto entity) {
        noteDao.update(toEntity(entity));
    }

    @Override
    public Long insert(NoteDto entity) {
        return noteDao.insert(toEntity(entity));
    }

    @Override
    public void remove(Long key) {
        noteDao.remove(key);
    }

}
