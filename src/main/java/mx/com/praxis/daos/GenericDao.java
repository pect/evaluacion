package mx.com.praxis.daos;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<E extends Serializable, K> {

    List<E> findAll(int page, int size);
    
    E findById(K key);
    
    void update(E entity);
    
    K insert(E entity);
    
    void remove(K key);
    
}
