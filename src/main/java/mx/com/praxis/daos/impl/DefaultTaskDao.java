package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.entities.Task;

@Stateless
public class DefaultTaskDao implements TaskDao {
    
    @PersistenceContext(name = "DBUnit")
    EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> findAll(int page, int size) {
        Query query = entityManager.createNamedQuery("Task.findAll");
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        
        List<Task> result = query.getResultList();
        return result;
    }

    @Override
    public Task findById(Long key) {
        Task entity = entityManager.find(Task.class, key);
        
        return entity;
    }

    @Override
    public void update(Task entity) {
        Task dbEntity = entityManager.find(Task.class, entity.getId());
        dbEntity.setTitle(entity.getTitle());
        dbEntity.setCompleted(entity.isCompleted());
        
        entityManager.merge(dbEntity);
    }

    @Override
    public Long insert(Task entity) {
        entityManager.persist(entity);
        
        return entity.getId();
    }

    @Override
    public void remove(Long key) {
        entityManager.remove(entityManager.find(Task.class, key));
    }

}
