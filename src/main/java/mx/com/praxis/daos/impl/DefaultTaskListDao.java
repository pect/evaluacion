package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.praxis.daos.TaskListDao;
import mx.com.praxis.entities.TaskList;

@Stateless
public class DefaultTaskListDao implements TaskListDao {
    
    @PersistenceContext(name = "DBUnit")
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<TaskList> findAll(int page, int size) {
        Query query = entityManager.createNamedQuery("TaskList.findAll");
        query.setFirstResult((page -1) * size);
        query.setMaxResults(size);
        
        List<TaskList> list = query.getResultList();
        return list;
    }

    @Override
    public TaskList findById(Long key) {
        TaskList result = entityManager.find(TaskList.class, key);

        return result;
    }

    @Override
    public void update(TaskList entity) {
        TaskList dbEntity = entityManager.find(TaskList.class, entity.getId());
        dbEntity.setTitle(entity.getTitle());
        
        entityManager.merge(dbEntity);
    }

    @Override
    public Long insert(TaskList entity) {
        entityManager.persist(entity);
        
        return entity.getId();
    }

    @Override
    public void remove(Long key) {
        entityManager.remove(entityManager.find(TaskList.class, key));
    }

}
