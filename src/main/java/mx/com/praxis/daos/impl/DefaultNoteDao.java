package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.praxis.daos.NoteDao;
import mx.com.praxis.entities.Note;

@Stateless
public class DefaultNoteDao implements NoteDao {
    
    @PersistenceContext(name = "DBUnit")
    EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Note> findAll(int page, int size) {
        Query query = entityManager.createNamedQuery("Note.findAll");
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        
        List<Note> result = query.getResultList();
        return result;
    }

    @Override
    public Note findById(Long key) {
        Note entity = entityManager.find(Note.class, key);
        
        return entity;
    }

    @Override
    public void update(Note entity) {
        Note dbEntity = entityManager.find(Note.class, entity.getId());
        dbEntity.setContent(entity.getContent());
        
        entityManager.merge(dbEntity);
    }

    @Override
    public Long insert(Note entity) {
        entityManager.persist(entity);
        
        return entity.getId();
    }

    @Override
    public void remove(Long key) {
        entityManager.remove(entityManager.find(Note.class, key));
    }

}
