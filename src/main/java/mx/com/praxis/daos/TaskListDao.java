package mx.com.praxis.daos;

import mx.com.praxis.entities.TaskList;

public interface TaskListDao extends GenericDao<TaskList, Long> {

}
