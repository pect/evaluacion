package mx.com.praxis.daos;

import mx.com.praxis.entities.Note;

public interface NoteDao extends GenericDao<Note, Long>{

}
