package mx.com.praxis.daos;

import mx.com.praxis.entities.Task;

public interface TaskDao extends GenericDao<Task, Long> {

}
