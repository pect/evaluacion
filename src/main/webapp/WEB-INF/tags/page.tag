<%@tag description="Layout" pageEncoding="UTF-8"%>
<%@attribute name="title" fragment="true"%>
<%@attribute name="content" fragment="true"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="with=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font-awesome/css/all.min.css">
	
	<title><jsp:invoke fragment="title"/></title>
</head>
<body>
	<h1><jsp:invoke fragment="title"/></h1>
	<jsp:invoke fragment="content"/>
	<hr>
	Praxis 2020 (c)d
</body>
</html>