<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:page>
	<jsp:attribute name="title">Note ${note.id}</jsp:attribute>
	
	<jsp:attribute name="content">
		<a href="${pageContext.request.contextPath}/lists">Lists/</a>
		<a href="${pageContext.request.contextPath}/lists?id=${note.task.taskList.id}">${note.task.taskList.id}/</a>
		<a href="${pageContext.request.contextPath}/tasks?id=${note.task.id}">Task ${note.task.id}/</a>
		
		<form action="${pageContext.request.contextPath}/notes" method="POST">
			<br><label><i class="fas fa-comment"></i> Update Note</label><br>
			<textarea name="content" id="content" rows="4">${note.content}</textarea>
			<input type="hidden" name="id" value="${note.id}" />
			<input type="hidden" name="id_task" value="${note.task.id}" />
			<input type="hidden" name="action" id="action" value="UPDATE"/>
					
			<button type="submit" class="fas fa-edit"></button>
		</form>
		<hr>
		<form action="${pageContext.request.contextPath}/notes" method="POST">
			<label>Remove Note</label>
			<input type="hidden" name="id" value="${note.id}" />
			<input type="hidden" name="id_task" value="${note.task.id}" />
			<input type="hidden" name="action" id="action" value="REMOVE"/>
			
			<button type="submit" class="fas fa-trash"></button>
		</form>
	</jsp:attribute>
</t:page>