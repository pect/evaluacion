<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:page>
	<jsp:attribute name="title">List ${list.id} : ${list.title}</jsp:attribute>
	
	<jsp:attribute name="content">
		<a href="${pageContext.request.contextPath}/lists">Lists</a>
		<ul>
			<c:forEach var="task" items="${list.tasks }">
				<li>
					<a href="${pageContext.request.contextPath}/tasks?id=${task.id}"><i class="fas fa-thumbtack"></i> 
						${task.id} 
					</a>
					${task.title}
					<i class="fas fa-${task.completed ? 'check' : 'spinner'}"></i>
				</li>
			</c:forEach>
		</ul>
		<hr>
		<form action="${pageContext.request.contextPath}/tasks" method="POST">
			<label>Add Task</label><br>
			<input type="text" name="title" placeholder="Title"/> <br>
			Is Completed: <input type="checkbox" name="isCompleted" id="isCompleted" value="true"/>
			<input type="hidden" name="id_list" value="${list.id}" />
			<input type="hidden" name="action" id="action" value="CREATE"/>
					
			<button type="submit" class="fas fa-plus-circle"></button>
		</form>
		<hr>
		<form action="${pageContext.request.contextPath}/lists" method="POST">
			<label>Update List</label><br>
			<input type="text" name="title" id="title" value="${list.title}"/>
			<input type="hidden" name="id" value="${list.id}" />
			<input type="hidden" name="action" id="action" value="UPDATE"/>
			
			<button type="submit" class="fas fa-edit"></button>
		</form>
		<hr>
		<form action="${pageContext.request.contextPath}/lists" method="POST">
			<label>Delete List</label>
			<input type="hidden" name="id" value="${list.id}" />
			<input type="hidden" name="action" id="action" value="REMOVE"/>
			
			<button type="submit" class="fas fa-trash"></button>
		</form>
	</jsp:attribute>
</t:page>