<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>


<t:page>
	<jsp:attribute name="title">Lists</jsp:attribute>
	
	<jsp:attribute name="content">
		<div>
			<ul>
				<c:forEach var="list" items="${lists}">
					<li id="${list.id}"><a href="${pageContext.request.contextPath}/lists?id=${list.id}">
						<i class="fas fa-list"></i>${list.id}</a>  - ${list.title}</li>
				</c:forEach>
			</ul>
			<form action="${pageContext.request.contextPath}/lists" method="POST">
				<input type="text" name="title" id="title" placeholder="Title"/>
				<input type="hidden" name="action" id="action" value="CREATE"/>
					
				<button type="submit"><i class="fas fa-plus-circle"></i>Save</button>
			</form>
		</div>
		
	</jsp:attribute>
</t:page>