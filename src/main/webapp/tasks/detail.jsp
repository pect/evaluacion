<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:page>
	<jsp:attribute name="title">Task ${task.id} : ${task.title}</jsp:attribute>
	
	<jsp:attribute name="content">
		<a href="${pageContext.request.contextPath}/lists">Lists/</a>
		<a href="${pageContext.request.contextPath}/lists?id=${task.taskList.id}">${task.taskList.id}/</a>
		<ul>
			<c:forEach var="note" items="${task.notes }">
				<li>
					<a href="${pageContext.request.contextPath}/notes?id=${note.id}"><i class="fas fa-comments"></i> 
						${note.id} 
					</a>
					${note.content}
				</li>
			</c:forEach>
		</ul>
		<hr>
		<form action="${pageContext.request.contextPath}/notes" method="POST">
			<label>Add Note</label><br>
			<textarea name="content" id="content" rows="4"></textarea>
			<input type="hidden" name="id_task" value="${task.id}" />
			<input type="hidden" name="action" id="action" value="CREATE"/>
					
			<button type="submit" class="fas fa-plus-circle"></button>
		</form>
		<hr>
		<form action="${pageContext.request.contextPath}/tasks" method="POST">
			<label>Update Task</label><br>
			<input type="text" name="title" id="title" value="${task.title}"/>
			Is Completed: <input type="checkbox" name="isCompleted" id="isCompleted" value="true" ${task.completed ? 'checked' : ''}/>
			<input type="hidden" name="id" value="${task.id}" />
			<input type="hidden" name="id_list" value="${task.taskList.id}" />
			<input type="hidden" name="action" id="action" value="UPDATE"/>
				
			<button type="submit" class="fas fa-edit"></button>
		</form>
		<hr>
		<form action="${pageContext.request.contextPath}/tasks" method="POST">
			<label>Remove Task</label>
			<input type="hidden" name="id" value="${task.id}" />
			<input type="hidden" name="id_list" value="${task.taskList.id}" />
			<input type="hidden" name="action" id="action" value="REMOVE"/>
			
			<button type="submit" class="fas fa-trash"></button>
		</form>
	</jsp:attribute>
</t:page>